use std::collections::BTreeMap;
use rand::{seq::SliceRandom, SeedableRng};
use rand_pcg;

fn main() {
    let mut rng = rand_pcg::Pcg32::seed_from_u64(42);
    let mut pool : Vec<String> = vec!["Rainbow".into(), "RoseGoldFuscia".into(), "Green".into(), "Cardinal".into()];

    let sorted : BTreeMap<u8, String> = pool.iter().map(|o| {
        let sum = o.chars().fold(42, |acc : u8, x| acc ^ x as u8);
        (sum, o.clone())
    }).collect();


    let choices : Vec<(String, String)> = sorted.iter().map(|(_, color)| {
        let choices : Vec<String> = pool.iter().filter(|x| x != &color).cloned().collect();
        let result : &String = choices.choose(&mut rng).unwrap();
        pool = pool.iter().filter(|x| x != &result).cloned().collect();
        (color.to_owned(), result.to_owned())
    }).collect();

    println!("");
    println!("Official Choices");
    println!("");

    for (family, target) in choices.iter() {
        println!("{} --> {}", family, target);
    }

    println!("");
}
